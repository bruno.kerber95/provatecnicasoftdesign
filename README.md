<img src = "https://gitlab.com/bruno.kerber95/provatecnicasoftdesign/uploads/378b52f0b8de40b153cd2e4cf396a8a3/rest-assured-logo-1.png" width="300px" />

<h1><b>Testes de API Rest com Rest-Assured</b></h1>

Estre projeto é referente a testes e validações em uma API REST de Gerenciamento de Simulações e Restrições Cadastrais. Estas validações foram realizadas nos verbos GET, POST, PUT e DELETE <b>(CRUD)</b> conforme documenção disponibilizada pela <i>SoftDesign</i>.

<h3><b>Pré-requisitos para rodar o projeto:</h3></b>

Para rodar o projeto, se faz necessária a instalação dos seguintes itens:

- Java;
- JDK;
- Maven;

|   JDK    | JRE       | MAVEN  |
| :---:    | :---:     | :---:  |
| 1.8.0_301| 1.8.0_301 |  3.8.2 |

<b>Obs.:</b> <i> Para que o projeto funcione corretamente é necessário que a vesão seja a mesma da listagem acima, pois qualquer versão diferente, poderá trazer incompatibilidade na execução da aplicação e na execução dos testes.</i>

<h3><b>Para rodar o projeto:</h3></b>

- Baixar o projeto disponibilizado pela SoftDesign <a href="https://drive.google.com/drive/folders/1035bC4qjqINNAH95aNXMrSsKAhroK-az"><br>Clique aqui!</a>
- Após o download do arquivo, colocar a pasta na raiz do seu disco local <b><i>C:\\</i></b>
- Logo em seguida, pelo Prompt de Comando, navegue até a pasta do projeto: <b><i>C:\prova-tecnica-api-master</i></b>
- Com o Prompt ainda aberto no diretório do projeto, execute o seguinte comando: <b><i>mvn clean spring-boot:run</i></b>

<br><b>Obs.:</b> Caso a porta 8080 esteja sendo utilizada para uma outra aplicação, você poderar alterar
a porta da aplicação adicionando a propriedade JVM server.port através do seguinte comando: <b><i>mvn clean spring-boot:run -Dserver.port=8888</i></b>

<h3><b>Documentação Técnica:</h3></b>

- Com o projeto em execução, a documentação técnica da API (OpenAPI/Swagger) estará disponível no seguinte link:
  <a href="http://localhost:8080/swagger-ui.html">
  <br>Clique aqui!</a>

<br><b>Obs.:</b> <i>Caso tenha alterado a porta do projeto na etapa anterior, basta editar a porta de 8080 para a porta que você está utilizando.</i>