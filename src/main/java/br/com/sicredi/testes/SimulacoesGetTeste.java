package br.com.sicredi.testes;

import br.com.sicredi.core.BaseTeste;
import io.restassured.http.ContentType;
import org.apache.http.HttpStatus;
import org.junit.Test;

import static io.restassured.RestAssured.given;
import static org.hamcrest.Matchers.*;
import static org.hamcrest.Matchers.containsString;

public class SimulacoesGetTeste extends BaseTeste {

    @Test
    public void listaSimulacoesCadastradas() {
        given()
            .urlEncodingEnabled(false)
            .contentType(ContentType.JSON)
        .when()
            .get("/simulacoes/")
        .then()
           .statusCode(HttpStatus.SC_OK)
           .body(is(not(nullValue())))
           .body(containsString("id"))
           .body(containsString("nome"))
           .body(containsString("cpf"))
           .body(containsString("email"))
           .body(containsString("valor"))
           .body(containsString("parcelas"))
           .body(containsString("seguro"));
    }

    @Test
    public void validaSimulacoesInexistentes() {
        given()
            .contentType(ContentType.JSON)
        .when()
            .get("/simulacoes/")
        .then()
            .body(is(not(nullValue())))
            .statusCode(HttpStatus.SC_OK)
            .body(is(not(containsString("[]"))));
            System.out.println("Existem simulações cadastradas");

    }

    @Test
    public void validandoSimulacoesCadastradasComCpfExistente() {
        given()
            .contentType(ContentType.JSON)
        .when()
            .get("/simulacoes/17822386034")
        .then()
            .statusCode(HttpStatus.SC_OK)
            .body(is(not(nullValue())))
            .body(containsString("id"))
            .body("id", is(notNullValue()))
            .body(containsString("nome"))
            .body("nome", is(notNullValue()))
            .body(containsString("email"))
            .body("email", is(notNullValue()))
            .body(containsString("valor"))
            .body("valor", is(notNullValue()))
            .body(containsString("parcelas"))
            .body("parcelas", is(notNullValue()))
            .body(containsString("seguro"))
            .body("seguro", is(notNullValue()));


    }

    @Test
    public void validandoSimulacoesCadastradasComCpfInexistente() {
        given()
        .contentType(ContentType.JSON)
        .when()
            .get("/simulacoes/1717566034")
        .then()
            .statusCode(HttpStatus.SC_NOT_FOUND)
            .body(is(not(nullValue())));

    }
}
