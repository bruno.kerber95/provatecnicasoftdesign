package br.com.sicredi.testes;

import br.com.sicredi.core.BaseTeste;
import io.restassured.http.ContentType;
import org.apache.http.HttpStatus;
import org.junit.Test;

import static io.restassured.RestAssured.given;
import static org.hamcrest.Matchers.*;
import static org.hamcrest.Matchers.containsString;

public class SimulacoesDeleteTeste extends BaseTeste {

    @Test
    public void RemoveSimulacaoCadastrada() {
        given()
         .urlEncodingEnabled(false)
            .contentType(ContentType.JSON)
         .when()
             .delete("/simulacoes/11")
         .then()
            .statusCode(HttpStatus.SC_OK)
            .body(is(not(nullValue())))
            .body(containsString("OK"));
    }

    @Test
    public void RemoveSimulacaoInexistente() {
        given()
                .urlEncodingEnabled(false)
                .contentType(ContentType.JSON)
                .when()
                .delete("/simulacoes/85")
                .then()
                .statusCode(HttpStatus.SC_NOT_FOUND)
                .body(is(not(nullValue())))
                .body(containsString("Simulação não encontrada"));
    }
}
