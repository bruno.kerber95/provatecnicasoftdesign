package br.com.sicredi.testes;

import br.com.sicredi.core.BaseTeste;
import io.restassured.http.ContentType;
import org.apache.http.HttpStatus;
import org.junit.Test;

import static io.restassured.RestAssured.given;
import static org.hamcrest.Matchers.*;

public class RestricoesGetTeste extends BaseTeste {

	@Test
	public void validaCpfRestricao() {
		given()
			.contentType(ContentType.JSON)
		.when()
			.get("/restricoes/97093236014")
		.then()
			.statusCode(HttpStatus.SC_OK)
			.body(is(not(nullValue())))
			.body(containsString("O CPF 97093236014 possui restri��o"));
	}


	@Test
	public void validaCpfSemRestricao() {
		given()
				.contentType(ContentType.JSON)
		.when()
			.get("/restricoes/84681551015")
		.then()
			.statusCode(HttpStatus.SC_NO_CONTENT)
			.body(is(("")));
	}
}	