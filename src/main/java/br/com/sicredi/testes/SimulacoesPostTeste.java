package br.com.sicredi.testes;

import br.com.sicredi.core.BaseTeste;
import br.com.sicredi.dados.DadosSimulacoes;
import br.com.sicredi.dados.GeradorDeDadosRandomicos;
import io.restassured.http.ContentType;
import io.restassured.path.json.JsonPath;
import io.restassured.response.Response;
import org.apache.http.HttpStatus;
import org.junit.Test;

import java.util.Map;

import static io.restassured.RestAssured.given;
import static org.hamcrest.Matchers.containsString;
import static org.junit.Assert.assertEquals;


public class SimulacoesPostTeste extends BaseTeste {
	DadosSimulacoes dadosSimulacoes = new DadosSimulacoes();
	GeradorDeDadosRandomicos geradorDeDadosRandomicos = new GeradorDeDadosRandomicos();

	@Test
	public void incluindoSimulacoes () {
		Map dados = dadosSimulacoes.dadosParaCadastroDeSimulacoes();
		Response response = given()
			.contentType(ContentType.JSON)
			.body(dados)
		.when()
			.post("/simulacoes")
		.then()
		.log().all()
		.extract().response();

		JsonPath jp = response.jsonPath();
		assertEquals(HttpStatus.SC_CREATED, response.statusCode());
		assertEquals(dados.get("dados"), jp.getString("dados"));

	}
	
	@Test
	public void validandoCpfDuplicado () {
		Map dados = dadosSimulacoes.dadosParaCadastroDeSimulacoes();
		dados.put("cpf", "66414919004");
		Response response = given()
			.contentType(ContentType.JSON)
			.body(dados)
		.when()
			.post("/simulacoes")
		.then()
			.extract().response();

		JsonPath jp = response.jsonPath();
		response.then().assertThat().body (containsString("CPF duplicado"));
		assertEquals(HttpStatus.SC_CONFLICT, response.statusCode());

	}
	
	@Test
	public void validandoCpfFormatoInvalido () {
		Map dados = dadosSimulacoes.dadosParaCadastroDeSimulacoes();
		dados.put("cpf", "408.534.550-82");
		Response response = given()
				.contentType(ContentType.JSON)
				.body(dados)
				.when()
				.post("/simulacoes")
				.then()
				.extract().response();

		JsonPath jp = response.jsonPath();
		assertEquals(HttpStatus.SC_BAD_REQUEST, response.statusCode());
	}
	
	@Test
	public void validandoInclusaoCpfEmBranco () {
		Map dados = dadosSimulacoes.dadosParaCadastroDeSimulacoes();
		dados.put("cpf", "");
		Response response = given()
				.contentType(ContentType.JSON)
				.body(dados)
				.when()
				.post("/simulacoes")
				.then()
				.extract().response();

		JsonPath jp = response.jsonPath();
		assertEquals(HttpStatus.SC_BAD_REQUEST, response.statusCode());

	}
	
	@Test
	public void validandoValorMenorQueMinimo () {
		Map dados = dadosSimulacoes.dadosParaCadastroDeSimulacoes();
		dados.put("valor", 999.99);
		Response response = given()
				.contentType(ContentType.JSON)
				.body(dados)
				.when()
				.post("/simulacoes")
				.then()
				.extract().response();

		JsonPath jp = response.jsonPath();
		assertEquals(HttpStatus.SC_BAD_REQUEST, response.statusCode());

	}
	
	@Test
	public void validandoValorIgualMinimo () {
		Map dados = dadosSimulacoes.dadosParaCadastroDeSimulacoes();
			dados.put("valor", 1000);
		Response response = given()
			.contentType(ContentType.JSON)
			.body(dados)
		.when()
			.post("/simulacoes")
		.then()
			.extract().response();

		JsonPath jp = response.jsonPath();
		//.statusCode(HttpStatus.SC_CREATED);
		assertEquals(HttpStatus.SC_CREATED, response.statusCode());
		assertEquals(dados.get("valor"), jp.getInt("valor"));
		
	}
	
	@Test
	public void validandoValorMaiorQueMaximo () {
		Map dados = dadosSimulacoes.dadosParaCadastroDeSimulacoes();
		dados.put("valor", 40000.01);
		Response response = given()
				.contentType(ContentType.JSON)
				.body(dados)
				.when()
				.post("/simulacoes")
				.then()
				.extract().response();

		JsonPath jp = response.jsonPath();
		response.then().assertThat().body (containsString("Valor deve ser menor ou igual a R$ 40.000"));
		assertEquals(HttpStatus.SC_BAD_REQUEST, response.statusCode());



	}
	
	@Test
	public void validandoValorIgualLimite () {
		Map dados = dadosSimulacoes.dadosParaCadastroDeSimulacoes();
		dados.put("valor", 40000);
		Response response = given()
				.contentType(ContentType.JSON)
				.body(dados)
				.when()
				.post("/simulacoes")
				.then()
				.extract().response();

		JsonPath jp = response.jsonPath();
		//.statusCode(HttpStatus.SC_CREATED);
		assertEquals(HttpStatus.SC_CREATED, response.statusCode());
		assertEquals(dados.get("valor"), jp.getInt("valor"));

	}

	@Test
	public void validandoSemInformarValor () {
		Map dados = dadosSimulacoes.dadosParaCadastroDeSimulacoes();
		dados.put("valor", "");
		Response response = given()
				.contentType(ContentType.JSON)
				.body(dados)
				.when()
				.post("/simulacoes")
				.then()
				.extract().response();

		JsonPath jp = response.jsonPath();
		response.then().assertThat().body (containsString("Valor n�o pode ser vazio"));
		assertEquals(HttpStatus.SC_BAD_REQUEST, response.statusCode());

	}
	
	@Test
	public void validandoQuantidadeParcelasMenorQueMinimo () {
		Map dados = dadosSimulacoes.dadosParaCadastroDeSimulacoes();
		dados.put("parcelas", 1);
		Response response = given()
				.contentType(ContentType.JSON)
				.body(dados)
				.when()
				.post("/simulacoes")
				.then()
				.extract().response();

		JsonPath jp = response.jsonPath();

		assertEquals(HttpStatus.SC_BAD_REQUEST, response.statusCode());
		//.body(containsString("Parcelas deve ser igual ou maior que 2"));

	}
	
	@Test
	public void validandoQuantidadeParcelasMaiorQueMinimo () {
		Map dados = dadosSimulacoes.dadosParaCadastroDeSimulacoes();
		dados.put("parcelas", 49);
		Response response = given()
				.contentType(ContentType.JSON)
				.body(dados)
				.when()
				.post("/simulacoes")
				.then()
				.extract().response();

		JsonPath jp = response.jsonPath();
		assertEquals(HttpStatus.SC_BAD_REQUEST, response.statusCode());

	}
	
	@Test
	public void validandoQuantidadeParcelasEmBranco () {
		Map dados = dadosSimulacoes.dadosParaCadastroDeSimulacoes();
		dados.put("parcelas", "");
		Response response = given()
				.contentType(ContentType.JSON)
				.body(dados)
				.when()
				.post("/simulacoes")
				.then()
				.extract().response();

		JsonPath jp = response.jsonPath();
		response.then().assertThat().body (containsString("Parcelas n�o pode ser vazio"));
		assertEquals(HttpStatus.SC_BAD_REQUEST, response.statusCode());

	}
	
	@Test
	public void validandoQuantidadeParcelasIgualAoMinimo () {
		Map dados = dadosSimulacoes.dadosParaCadastroDeSimulacoes();
		dados.put("parcelas", 2);
		Response response = given()
				.contentType(ContentType.JSON)
				.body(dados)
				.when()
				.post("/simulacoes")
				.then()
				.extract().response();

		JsonPath jp = response.jsonPath();
		assertEquals(HttpStatus.SC_CREATED, response.statusCode());
		assertEquals(dados.get("parcelas"), jp.getInt("parcelas"));
	}
	
	@Test
	public void validandoQuantidadeParcelasIgualAoMaximo () {
		Map dados = dadosSimulacoes.dadosParaCadastroDeSimulacoes();
		dados.put("parcelas", 48);
		Response response = given()
				.contentType(ContentType.JSON)
				.body(dados)
				.when()
				.post("/simulacoes")
				.then()
				.extract().response();

		JsonPath jp = response.jsonPath();
		assertEquals(HttpStatus.SC_CREATED, response.statusCode());
		assertEquals(dados.get("parcelas"), jp.getInt("parcelas"));

	}
	
	@Test
	public void validandoSeguroEmBranco () {
		Map dados = dadosSimulacoes.dadosParaCadastroDeSimulacoes();
		dados.put("seguro", "");
		Response response = given()
				.contentType(ContentType.JSON)
				.body(dados)
				.when()
				.post("/simulacoes")
				.then()
				.extract().response();

		JsonPath jp = response.jsonPath();
		assertEquals(HttpStatus.SC_BAD_REQUEST, response.statusCode());

	}
	
	@Test
	public void validandoNomeEmBranco () {
		Map dados = dadosSimulacoes.dadosParaCadastroDeSimulacoes();
		dados.put("nome", "");
		Response response = given()
				.contentType(ContentType.JSON)
				.body(dados)
				.when()
				.post("/simulacoes")
				.then()
				.extract().response();

		JsonPath jp = response.jsonPath();
		assertEquals(HttpStatus.SC_BAD_REQUEST, response.statusCode());
		assertEquals(dados.get("nome"), jp.getString("nome"));

	}
	
	@Test
	public void validandoEmailEmBranco () {
		Map dados = dadosSimulacoes.dadosParaCadastroDeSimulacoes();
		dados.put("email", "");
		Response response = given()
				.contentType(ContentType.JSON)
				.body(dados)
				.when()
				.post("/simulacoes")
				.then()
				.extract().response();

		JsonPath jp = response.jsonPath();
		response.then().assertThat().body (containsString("E-mail deve ser um e-mail v�lido"));
		assertEquals(HttpStatus.SC_BAD_REQUEST, response.statusCode());
		assertEquals(dados.get("email"), jp.getString("email"));

	}
	
	@Test
	public void validandoEmailInvalidoA () {
		Map dados = dadosSimulacoes.dadosParaCadastroDeSimulacoes();
		dados.put("email", "teste");
		Response response = given()
				.contentType(ContentType.JSON)
				.body(dados)
				.when()
				.post("/simulacoes")
				.then()
				.extract().response();

		JsonPath jp = response.jsonPath();
		assertEquals(HttpStatus.SC_BAD_REQUEST, response.statusCode());

	}
	
	@Test
	public void validandoEmailInvalidoB () {
		Map dados = dadosSimulacoes.dadosParaCadastroDeSimulacoes();
		dados.put("email", "teste@");
		Response response = given()
				.contentType(ContentType.JSON)
				.body(dados)
				.when()
				.post("/simulacoes")
				.then()
				.extract().response();

		JsonPath jp = response.jsonPath();
		assertEquals(HttpStatus.SC_BAD_REQUEST, response.statusCode());

	}
	
	@Test
	public void validandoEmailInvalidoC () {
		Map dados = dadosSimulacoes.dadosParaCadastroDeSimulacoes();
		dados.put("email", "@terra.com");
		Response response = given()
				.contentType(ContentType.JSON)
				.body(dados)
				.when()
				.post("/simulacoes")
				.then()
				.extract().response();

		JsonPath jp = response.jsonPath();
		assertEquals(HttpStatus.SC_BAD_REQUEST, response.statusCode());

	}
}