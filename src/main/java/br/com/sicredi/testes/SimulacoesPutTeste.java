package br.com.sicredi.testes;

import br.com.sicredi.core.BaseTeste;
import br.com.sicredi.dados.DadosSimulacoes;
import br.com.sicredi.dados.GeradorDeDadosRandomicos;
import io.restassured.http.ContentType;
import io.restassured.path.json.JsonPath;
import io.restassured.response.Response;
import org.apache.http.HttpStatus;
import org.junit.Test;

import java.util.Map;

import static io.restassured.RestAssured.given;
import static org.hamcrest.Matchers.*;
import static org.junit.Assert.assertEquals;

public class SimulacoesPutTeste extends BaseTeste {
	DadosSimulacoes dadosSimulacoes = new DadosSimulacoes();
	GeradorDeDadosRandomicos geradorDeDadosRandomicos = new GeradorDeDadosRandomicos();

	@Test
	public void gerandoMassaparaTesteA () {
		Map dados = dadosSimulacoes.dadosParaCadastroDeSimulacoes();
		dados.put("cpf", "11538468026");
		Response response = given()
				.contentType(ContentType.JSON)
				.body(dados)
				.when()
				.post("/simulacoes")
				.then()
				.log().all()
				.extract().response();

		JsonPath jp = response.jsonPath();
		assertEquals(HttpStatus.SC_CREATED, response.statusCode());
	}

	@Test
	public void gerandoMassaparaTesteB () {
		Map dados = dadosSimulacoes.dadosParaCadastroDeSimulacoes();
		dados.put("cpf", "81960459058");
		Response response = given()
				.contentType(ContentType.JSON)
				.body(dados)
				.when()
				.post("/simulacoes")
				.then()
				.log().all()
				.extract().response();

		JsonPath jp = response.jsonPath();
		assertEquals(HttpStatus.SC_CREATED, response.statusCode());
	}

	@Test
	public void gerandoMassaparaTesteC () {
		Map dados = dadosSimulacoes.dadosParaCadastroDeSimulacoes();
		dados.put("cpf", "52346089052");
		Response response = given()
				.contentType(ContentType.JSON)
				.body(dados)
				.when()
				.post("/simulacoes")
				.then()
				.log().all()
				.extract().response();

		JsonPath jp = response.jsonPath();
		assertEquals(HttpStatus.SC_CREATED, response.statusCode());
	}

	@Test
	public void AlterandoValor () {
		given()
			.contentType(ContentType.JSON)
			.body("    {\r\n"
					+ "        \"id\": 17,\r\n"
					+ "        \"nome\": \"Deltrano\",\r\n"
					+ "        \"cpf\": \"84859318073\",\r\n"
					+ "        \"email\": \"deltrano@gmail.com\",\r\n"
					+ "        \"valor\": 5000,\r\n"
					+ "        \"parcelas\": 5,\r\n"
					+ "        \"seguro\": false\r\n"
					+ "    }")
		.when()
			.put("/simulacoes/52346089052")
		.then()
			.statusCode(HttpStatus.SC_OK)
			.body("valor", is(5000))
			.body("valor", is(notNullValue()));
			
	}
	
	@Test
	public void AlterandoId () {
		given()
			.contentType(ContentType.JSON)
			.body("{\r\n"
					+ "    \"id\": 20,\r\n"
					+ "    \"nome\": \"Deltrano\",\r\n"
					+ "    \"cpf\": \"11538468026\",\r\n"
					+ "    \"email\": \"deltrano@gmail.com\",\r\n"
					+ "    \"valor\": 20000.00,\r\n"
					+ "    \"parcelas\": 5,\r\n"
					+ "    \"seguro\": false\r\n"
					+ "}")
		.when()
			.put("/simulacoes/11538468026")
		.then()
			.statusCode(HttpStatus.SC_OK)
			.body("id", is(20))
			.body("valor", is(notNullValue()));
			
	}
	
	@Test
	public void AlterandoCpf () {
		given()
			.contentType(ContentType.JSON)
			.body("{\r\n"
					+ "    \"id\": 11,\r\n"
					+ "    \"nome\": \"Fulano\",\r\n"
					+ "    \"cpf\": \"70109723031\",\r\n"
					+ "    \"email\": \"fulano@gmail.com\",\r\n"
					+ "    \"valor\": 11000.00,\r\n"
					+ "    \"parcelas\": 3,\r\n"
					+ "    \"seguro\": true\r\n"
					+ "}")
		.when()
			.put("/simulacoes/66414919004")
		.then()
			.statusCode(HttpStatus.SC_OK)
			.body("cpf", is("70109723031"))
			.body("cpf", is(notNullValue()));
			
	}

	@Test
	public void AlterandoCpfPassandoValorInexistenteNoEndpoint () {
		given()
			.contentType(ContentType.JSON)
			.body("{\r\n"
					+ "    \"id\": 11,\r\n"
					+ "    \"nome\": \"Fulano\",\r\n"
					+ "    \"cpf\": \"66414919004\",\r\n"
					+ "    \"email\": \"fulano@gmail.com\",\r\n"
					+ "    \"valor\": 11000.00,\r\n"
					+ "    \"parcelas\": 3,\r\n"
					+ "    \"seguro\": true\r\n"
					+ "}")
		.when()
			.put("/simulacoes/29466614070")
		.then()
			.statusCode(HttpStatus.SC_NOT_FOUND)
			.body(containsString("CPF 29466614070 não encontrado"));
			
	}

	@Test
	public void AlterandoQtdParcelas () {
		given()
			.contentType(ContentType.JSON)
			.body("{\n" +
					"    \"id\": 17,\n" +
					"    \"nome\": \"Deltrano\",\n" +
					"    \"cpf\": \"70109723031\",\n" +
					"    \"email\": \"deltrano@gmail.com\",\n" +
					"    \"valor\": 20000.00,\n" +
					"    \"parcelas\": 32,\n" +
					"    \"seguro\": false\n" +
					"}")
		.when()
			.put("/simulacoes/84859318073")
		.then()
			.statusCode(HttpStatus.SC_OK)
			.body("parcelas", is(32))
			.body("parcelas", is(notNullValue()));

	}

	@Test
	public void AlterandoNome () {
		given()
				.contentType(ContentType.JSON)
				.body("{\n" +
						"    \"id\": 17,\n" +
						"    \"nome\": \"Deltrano das Dores\",\n" +
						"    \"cpf\": \"11538468026\",\n" +
						"    \"email\": \"deltrano@gmail.com\",\n" +
						"    \"valor\": 20000.00,\n" +
						"    \"parcelas\": 3,\n" +
						"    \"seguro\": false\n" +
						"}")
				.when()
				.put("/simulacoes/11538468026")
				.then()
				.statusCode(HttpStatus.SC_OK)
				.body("nome", is("Deltrano das Doress"))
				.body("nome", is(notNullValue()));

	}

	@Test
	public void AlterandoSeguro () {
		given()
				.contentType(ContentType.JSON)
				.body("{\n" +
						"    \"id\": 17,\n" +
						"    \"nome\": \"Deltrano das Dores\",\n" +
						"    \"cpf\": \"11538468026\",\n" +
						"    \"email\": \"deltrano@gmail.com\",\n" +
						"    \"valor\": 20000.00,\n" +
						"    \"parcelas\": 3,\n" +
						"    \"seguro\": true\n" +
						"}")
				.when()
				.put("/simulacoes/11538468026")
				.then()
				.statusCode(HttpStatus.SC_OK)
				.body("seguro", is(true))
				.body("seguro", is(notNullValue()));

	}


}

