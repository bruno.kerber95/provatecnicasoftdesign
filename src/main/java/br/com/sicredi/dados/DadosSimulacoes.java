package br.com.sicredi.dados;

import br.com.sicredi.core.BaseTeste;
import java.util.HashMap;
import java.util.Map;


public class DadosSimulacoes extends BaseTeste {
    GeradorDeDadosRandomicos dados = new GeradorDeDadosRandomicos();

    public Map dadosParaCadastroDeSimulacoes() {
        Map<String, Object> parametros = new HashMap<String, Object>();

        parametros.put("nome", dados.gerarNomesValidos());
        parametros.put("cpf", dados.gerarCpfValido());
        parametros.put("email", dados.gerarEmailValido());
        parametros.put("valor", dados.gerarValorValido());
        parametros.put("parcelas", dados.gerarParcelasValidas());
        parametros.put("seguro", dados.gerarSeguroValido());

        return parametros;
    }

    public Map dadosCpf() {
        Map<String, Object> parametros = new HashMap<String, Object>();

        parametros.put("cpf", dados.gerarCpfValido());

        return  parametros;
    }
}