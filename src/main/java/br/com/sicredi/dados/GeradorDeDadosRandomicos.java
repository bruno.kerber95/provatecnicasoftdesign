package br.com.sicredi.dados;

import com.github.javafaker.Faker;

import java.util.Locale;
import java.util.Random;


public class GeradorDeDadosRandomicos {

   Faker faker = new Faker (new Locale("pt-BR"));

   Random dadoRandomico = new Random();

     public String gerarNomesValidos() {
         String nomeCompleto = faker.name().fullName();
        return nomeCompleto;
    }

    public String gerarCpfValido(){
        String[] listaCpf = {"65540210000", "43959374038", "44656181072", "07304872020", "12836994002", "19921931008", "66218230023", "07080732042", "70222145064", "45963262070", "05994563021", "29357645012", "41618488031", "66080060043", "62536144011", "77703453080", "09195671005", "99703863019", "80117940003","64020020035","64020020035", "96422793049", "41868739090", "33056254060"};

        return listaCpf[dadoRandomico.nextInt(listaCpf.length)];
    }

    public String gerarEmailValido(){
        String listaEmail = faker.internet().emailAddress();

        return listaEmail;
    }

    public String gerarValorValido(){
        String[] listaValor = {"10000", "3000", "1000", "1200", "40000", "3380.25", "2110.08", "3550.08"};

        return listaValor[dadoRandomico.nextInt(listaValor.length)];
    }

    public String gerarSeguroValido(){
        String[] listaSeguro = {"true", "false"};

        return listaSeguro[dadoRandomico.nextInt(listaSeguro.length)];
    }

    public String gerarParcelasValidas(){
        String[] listaParcela = {"2", "3", "10", "15", "22", "29", "37", "44", "47", "48"};

        return listaParcela[dadoRandomico.nextInt(listaParcela.length)];
    }
}

