package br.com.sicredi.core;

import io.restassured.http.ContentType;

public interface Constantes {

	String URL = "http://localhost";
	Integer PORTA = 8080;
	String CAMINHO = "/api/v1";

	ContentType APP_CONTENT_TYPE = ContentType.JSON;
}
