package br.com.sicredi.core;


import org.junit.BeforeClass;

import io.restassured.RestAssured;
import io.restassured.builder.RequestSpecBuilder;

public class BaseTeste implements Constantes {

	@BeforeClass

	public static void setup() {
		RestAssured.baseURI = URL;
		RestAssured.port = PORTA;
		RestAssured.basePath = CAMINHO;
		
		RequestSpecBuilder reqBuilder = new RequestSpecBuilder();
		reqBuilder.setContentType(APP_CONTENT_TYPE);
		
		RestAssured.enableLoggingOfRequestAndResponseIfValidationFails();
	}
}
